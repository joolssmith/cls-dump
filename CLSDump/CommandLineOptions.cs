﻿using CommandLine;
using CommandLine.Text;
using System;

namespace CLSDump
{
    class CommandLineOptions
    {
        [Option('t', "tenant", Required = true, HelpText = "FlexNet Operations tenant")]
        public String Tenant { get; set; }

        [Option('s', "server", Required = true, HelpText = "Cloud license server ID")]
        public string ClsId { get; set; }

        [Option('p', "password", Required = true, HelpText = "Cloud license server admin password")]
        public String Password { get; set; }

        [ParserState]
        public IParserState LastParserState { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            return HelpText.AutoBuild(this,
              current => HelpText.DefaultParsingErrorsHandler(this, current));
        }
    }
}
