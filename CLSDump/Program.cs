﻿using CommandLine;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;

namespace CLSDump
{
    class Program
    {
        static JsonSerializerSettings settings = new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            NullValueHandling = NullValueHandling.Include
        };

        static void Main(string[] args)
        {
            try
            {
                var options = new CommandLineOptions { };

                if (Parser.Default.ParseArguments(args, options))
                {
                    // https://flex1341.compliance.flexnetoperations.com/api/1.0/instances/D4YP2ZCDT8R8
                    // "Smiths@123"

                    var url = String.Format("https://{0}.compliance.flexnetoperations.com/api/1.0/instances/{1}",
                        options.Tenant,
                        options.ClsId);

                    var client = new RestClient(url);

                    var request = new RestRequest
                    {
                        Method = Method.POST,
                        Resource = "authorize",
                        RequestFormat = DataFormat.Json
                    };
                    request.Parameters.Clear();
                    request.AddHeader("Accept", "application/json");
                    request.AddJsonBody(new
                    {
                        password = options.Password,
                        user = "admin"
                    });
                    var response = client.Execute(request);

                    if (response.IsSuccessful)
                    {
                        var deserial = new JsonDeserializer();

                        var token = deserial.Deserialize<Token>(response);

                        request = new RestRequest
                        {
                            Method = Method.GET,
                            Resource = "features",
                            RequestFormat = DataFormat.Json
                        };
                        request.Parameters.Clear();
                        request.AddHeader("Authorization", "Bearer " + token.token);

                        response = client.Execute(request);

                        if (response.IsSuccessful)
                        {
                            var lines = JsonConvert.DeserializeObject<List<Feature>>(response.Content, settings);

                            var features = deserial.Deserialize<List<Feature>>(response);

                            var formatT = "+--------------------+-------+------+----------+---------+-----+----------+----------+----------+";
                            var formatH = "|{0,-20}|{1,-7}|{2,6}|{3,10}|{4,9}|{5,5}|{6,10}|{7,10}|{8,10}|";
                            var formatM = "+--------------------+-------+------+----------+---------+-----+----------+----------+----------+";
                            var formatL = "|{0,-20}|{1,7}|{2,6}|{3,10}|{4,9}|{5,5}|{6,10}|{7,10}|{8,10}|";
                            var formatB = "+--------------------+-------+------+----------+---------+-----+----------+----------+----------+";

                            Console.WriteLine("Feature usage for flex1341 D4YP2ZCDT8R8");
                            Console.WriteLine(formatT);
                            Console.WriteLine(formatH,
                                "Feature",
                                "Version",
                                "Count",
                                "Type",
                                "Overdraft",
                                "Used",
                                "Start Date",
                                "Expiration",
                                "Ent Expire");

                            foreach (var feature in features)
                            {
                                Console.WriteLine(formatM);
                                Console.WriteLine(formatL,
                                    feature.featureName,
                                    feature.featureVersion,
                                    feature.featureCount,
                                    feature.metered ? "metered" : "concurrent",
                                    feature.overdraftCount == "0" ? "" : feature.overdraftCount,
                                    feature.used == 0 ? "" : feature.used.ToString(),
                                    feature.starts.ToString("yyyy-MM-dd"),
                                    feature.expiry.ToString("yyyy-MM-dd"),
                                    feature.entitlementExpiration == DateTime.MinValue ? "" : feature.entitlementExpiration.ToString("yyyy-MM-dd"));

                            }
                            Console.WriteLine(formatB);
                            Console.WriteLine();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
