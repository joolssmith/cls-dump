﻿using System;

namespace CLSDump
{
    class Token
    {
        public DateTime expires { get; set; }
        public String token { get; set; }
    }
}
