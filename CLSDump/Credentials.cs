﻿using System;

namespace CLSDump
{
    class Credentials
    {
        public String password { get; set; }
        public String user { get; set; }
    }
}
