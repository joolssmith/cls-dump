﻿using System;

namespace CLSDump
{
    class Feature
    {
        public String id { get; set; }
        public String type { get; set; }
        public String featureName { get; set; }
        public String featureVersion { get; set; }
        public DateTime expiry { get; set; }
        public String featureCount { get; set; }
        public String overdraftCount { get; set; }
        public int used { get; set; }
        public String issuer { get; set; }
        public DateTime issued { get; set; }
        public String featureId { get; set; }
        public DateTime starts { get; set; }
        public DateTime entitlementExpiration { get; set; }
        public String featureKind { get; set; }
        public String vendor { get; set; }
        public int meteredUndoInterval { get; set; }
        public Boolean meteredReusable { get; set; }
        public DateTime receivedTime { get; set; }
        public String concurrent { get; set; }
        public Boolean uncounted { get; set; }
        public Boolean uncappedOverdraft { get; set; }
        public Boolean metered { get; set; }
        public String reserved { get; set; }

    }
}
